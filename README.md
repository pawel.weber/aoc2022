# Thaumatec Advent of Code challenge

### What is it about?
Hello everyone, we invite you to Thaumatec AoC challenge.

Advent of Code is an annual Christmas challenge where between 1st and 24th of December every day new  puzzle is published.
Each puzzle contains of two part programming problem to be solved. 

It is a great opportunity to learn and experiment with a technology you are not familiar with - you'll have interesting problems to solve but no risk beside solving puzzles a little longer.

You can write a program that solves the puzzle in any language you want since problems don't have any external dependencies like external code to use, libraries to link, operating system dependent elements etc.

Each day, you download your input text file, process it using your program and enter the answer on the website.


### Submissions
Event will launch 1st December, 5 am in our time zone. Please register at  [Advent of Code](https://adventofcode.com/), Click on a [Leaderboard] and join to a following private leaderbord: `769621-29dc304b`. 

Please create a public code repository for your solutions wherever you want.

Everybody participating in challenge, please fill in a table below (preferably issue a PR or send your submission to Chodzik Michałowicz on Thaumatec's Slack.

### Participation rewards
To be announced soon.

### List of participants
| User handle | Name | Repo | Programming Language |
|----------|:-------------:|------:|------|
| mchodzikiewicz |  Michał Chodzikiewicz | [gitlab](https://gitlab.com/mchodzikiewicz/aoc2022) | Rust |
| Bartosz Michalak |  Bartosz Michalak | [gitlab](https://gitlab.com/bartoszmichalak19/aoc-2022) | Rust |
| MarikaC |  Marika Chlebowska | [gitlab](https://gitlab.com/MarikaC/aoc-2022) | Rust/Python |
| Jakub Cebulski |  Jakub Cebulski | [gitlab](https://gitlab.com/biesmir/aoc-2022) | Rust |
| Witold Lipieta |  Witold Lipieta | [gitlab](https://gitlab.com/witold.lipieta/aoc2022) | Rust |

